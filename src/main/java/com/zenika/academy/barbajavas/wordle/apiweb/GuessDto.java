package com.zenika.academy.barbajavas.wordle.apiweb;

public class GuessDto {
    private String guess;

    public String getGuess() {
        return guess;
    }
}
