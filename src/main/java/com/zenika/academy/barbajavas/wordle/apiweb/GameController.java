package com.zenika.academy.barbajavas.wordle.apiweb;

import com.zenika.academy.barbajavas.wordle.application.GameManager;
import com.zenika.academy.barbajavas.wordle.domain.model.Game;
import com.zenika.academy.barbajavas.wordle.domain.repository.GameRepository;
import com.zenika.academy.barbajavas.wordle.domain.service.BadLengthException;
import com.zenika.academy.barbajavas.wordle.domain.service.IllegalWordException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class GameController {

    GameManager gameManager;
    GameRepository gameRepository;

    @Autowired
    public GameController(GameManager gameManager, GameRepository gameRepository) {
        this.gameManager = gameManager;
        this.gameRepository = gameRepository;
    }

    @GetMapping("/games")
    public void printHelloWorld() {
        System.out.println("Hello world!");
    }

    // Fonctionne sans ResponseEntity
    // @PostMapping("/games")
    // public Game postNewGame(@RequestParam(value="wordLength", defaultValue = "4") int wordLength, @RequestParam(value="maxAttempts", defaultValue = "4") int maxAttempts){
    //     return gameManager.startNewGame(wordLength, maxAttempts);
    // }

    @PostMapping("/games")
    public ResponseEntity<Game> postNewGame(@RequestParam(value = "wordLength", defaultValue = "4") int wordLength, @RequestParam(value = "maxAttempts", defaultValue = "4") int maxAttempts) {
        if (wordLength > 3 && maxAttempts > 2) {
            return new ResponseEntity<Game>(gameManager.startNewGame(wordLength, maxAttempts), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<Game>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/games/{gameTid}")
    public ResponseEntity<Game> postGuessWord(@PathVariable(value = "gameTid") String gameTid, @RequestBody GuessDto guess) throws BadLengthException, IllegalWordException {
        //System.out.println(gameTid);
        //System.out.println(guess.getGuess());
        if (gameRepository.findByTid(gameTid).isPresent()) {
            if (gameManager.attempt(gameTid, guess.getGuess()) != null) {
                return new ResponseEntity<Game>(gameManager.attempt(gameTid, guess.getGuess()), HttpStatus.OK);
            } else {
                return new ResponseEntity<Game>(HttpStatus.BAD_REQUEST); //400
            }
        } else {
            return new ResponseEntity<Game>(HttpStatus.NOT_FOUND); //404
        }
    }


    @GetMapping("/games/{gameTid}")
    public ResponseEntity<Game> getStateSpecificGame
            (@PathVariable(value = "gameTid") String gameTid) {
        if (gameRepository.findByTid(gameTid).isPresent()) {
            return new ResponseEntity<Game>(gameRepository.findByTid(gameTid).get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Game>(HttpStatus.NOT_FOUND);
        }
    }
}





