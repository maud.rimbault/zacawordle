package com.zenika.academy.barbajavas.wordle.domain.configuration;

import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {
    @Bean
    public I18n getI18n(@Value("${wordle-app.language}") String lang) throws Exception {
        return I18nFactory.getI18n(lang);
    }
}
